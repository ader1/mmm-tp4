package com.example.ader.tp1;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.annotation.TargetApi;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;

import android.content.IntentFilter;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.app.Activity;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import com.parse.FindCallback;
import com.parse.ParseObject;
import com.parse.ParseQuery;

/**
 * Created by ader on 07/10/15.
 */
public class MainActivityListView extends AppCompatActivity {
    private ArrayList<HashMap<String, String>> listItem = new ArrayList<HashMap<String, String>>();
    private ListView maListView;
    private SimpleAdapter mListAdapter;
    private ArrayList<HashMap<String, String>> listItemR;
    private ParseReceiver receiver;
    static final int IDENT_REQUEST = 1;
    EditText inputSearch;


    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.list_view);

        mListAdapter = new SimpleAdapter(getApplicationContext(), listItem, R.layout.item,
                new String[]{"nom", "prenom", "date", "ville","img"}, new int[]{R.id.nom, R.id.prenom, R.id.date, R.id.ville,R.id.img});

        maListView = (ListView) findViewById(R.id.listviewperso);
        maListView.setAdapter(mListAdapter);
        inputSearch = (EditText) findViewById(R.id.EditText01);
       // initialisebd();

        /* Exercice 2.3 TP4 */
        /*Enregistrer et demarer le Service  */
        IntentFilter filter = new IntentFilter(ParseReceiver.PROCESS_RESPONSE);
        filter.addCategory(Intent.CATEGORY_DEFAULT);
        receiver = new ParseReceiver();
        registerReceiver(receiver, filter);
        Intent intent = new Intent(this, ServiceUpdate.class);
        startService(intent);

        initialiseBDServer();

        inputSearch.addTextChangedListener(new TextWatcher() {
            @Override

            public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
                // si l'utilisateur change le texte
                if (cs.toString().equals("")) {
                    //rénitialisation de la listview
                    initialisebd();
                } else {
                    // lancer la recherche (filtrage par Nom utilisateur)
                    searchItem(cs.toString());
                }
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                          int arg3) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub
            }
        });

    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu1, menu);
        return true;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (id) {
            /* Excercie 2.2 TP4 : Bouton actualiser  */
            case R.id.action_refresh: {
                initialiseBDServer();
                break;
            }
        }
        return super.onOptionsItemSelected(item);
    }


        @Override
    protected void onRestart(){
        super.onRestart();
            initialiseBDServer();
    }
    @Override
    public void onStop(){
        super.onStop();
        unregisterReceiver(receiver);
    }

    protected void onActivityResult (int requestCode, int resultCode, Intent intent){
        //exercice 1
//        super.onActivityResult(requestCode, resultCode, intent);
//       // Récuperation du contenu de l'intent SecondeA
//        Bundle extras = intent.getExtras();
//        Utilisateur p = extras.getParcelable("Uti");
//        String prenom = p.getPrenom();
//        String nom = p.getNom();
//        String date = p.getDate();
//        String villee = p.getVille();
//        maListView = (ListView) findViewById(R.id.listviewperso);
//        //On déclare la HashMap qui contiendra les informations pour un item
//        HashMap<String, String> map;
//        if (listItem==null) {
//            //Création de la ArrayList qui nous permettra de remplire la listView
//            listItem = new ArrayList<HashMap<String, String>>();
//        }
//        //Création d'une HashMap pour insérer les informations du premier item de notre listView
//        map = new HashMap<String, String>();
//        //on insère un élément titre que l'on récupérera dans le textView titre créé dans le fichier affichageitem.xml
//        map.put("nom", nom);
//        //on insère un élément description que l'on récupérera dans le textView description créé dans le fichier affichageitem.xml
//        map.put("prenom", prenom);
//        //on insère un élément titre que l'on récupérera dans le textView titre créé dans le fichier affichageitem.xml
//        map.put("date", date);
//        //on insère un élément titre que l'on récupérera dans le textView titre créé dans le fichier affichageitem.xml
//        map.put("ville", villee);
//        //on insère la référence à l'image (convertit en String car normalement c'est un int) que l'on récupérera dans l'imageView créé dans le fichier affichageitem.xml
//        map.put("img", String.valueOf(R.mipmap.ic_launcher));
//        //enfin on ajoute cette hashMap dans la arrayList
//        listItem.add(map);
//        //Création d'un SimpleAdapter qui se chargera de mettre les items présent dans notre list (listItem) dans la vue list_view
//        if (mListAdapter==null) {
//            mListAdapter = new SimpleAdapter(this.getBaseContext(), listItem, R.layout.item,
//                    new String[]{"nom", "prenom", "date", "ville", "img"}, new int[]{R.id.nom, R.id.prenom, R.id.date, R.id.ville, R.id.img});
//            maListView.setAdapter(mListAdapter);
//        }
//        //On attribue à notre listView l'adapter que l'on vient de créer
//        mListAdapter.notifyDataSetChanged();

    }

    // fonction permettant l'initialisation de la listview à partir de la base de données
    public void initialisebd(){
        String columns[] = new String[] { ColumnsTable.Client.Client_NAME, ColumnsTable.Client.Client_PRENOM, ColumnsTable.Client.Client_DATE, ColumnsTable.Client.Client_VILLE };
        Uri mContacts = AndroidProvider.CONTENT_URI;
        Cursor cur = getContentResolver().query(mContacts, columns, null, null, null);
        listItem = new ArrayList<HashMap<String, String>>();
        mListAdapter = new SimpleAdapter(this.getBaseContext(), listItem, R.layout.item,
                new String[]{"nom", "prenom", "date", "ville","img"}, new int[]{R.id.nom, R.id.prenom, R.id.date, R.id.ville,R.id.img});
        maListView.setAdapter(mListAdapter);


        if (cur.moveToFirst())
            do {

                //On déclare la HashMap qui contiendra les informations pour un item
                HashMap<String, String> map;
                //Création de la ArrayList qui nous permettra de remplire la listView

                //Création d'une HashMap pour insérer les informations du premier item de notre listView
                map = new HashMap<String, String>();
                //on insère un élément titre que l'on récupérera dans le textView titre créé dans le fichier affichageitem.xml
                map.put("nom", cur.getString(cur.getColumnIndex(ColumnsTable.Client.Client_NAME)));
                //on insère un élément description que l'on récupérera dans le textView description créé dans le fichier affichageitem.xml
                map.put("prenom", cur.getString(cur.getColumnIndex(ColumnsTable.Client.Client_PRENOM)));
                //on insère un élément titre que l'on récupérera dans le textView titre créé dans le fichier affichageitem.xml
                map.put("date", cur.getString(cur.getColumnIndex(ColumnsTable.Client.Client_DATE)));
                //on insère un élément titre que l'on récupérera dans le textView titre créé dans le fichier affichageitem.xml
                map.put("ville",cur.getString(cur.getColumnIndex(ColumnsTable.Client.Client_VILLE)));
                //on insère la référence à l'image (convertit en String car normalement c'est un int) que l'on récupérera dans l'imageView créé dans le fichier affichageitem.xml
                map.put("img", String.valueOf(R.mipmap.ic_launcher));
                //enfin on ajoute cette hashMap dans la arrayList
                listItem.add(map);
                //Création d'un SimpleAdapter qui se chargera de mettre les items présent dans notre list (listItem) dans la vue affichageitem
                //if(mListAdapter==null){
                Log.d("e", "enter adapter");
                mListAdapter.notifyDataSetChanged();
            } while (cur.moveToNext());
        cur.close();
    }


  /*fonction permettant la recherche du mot saisit par l'utilisateur dans la listview (Filtrage des items par Nom utilisateur)*/
    public void searchItem(String s){
        listItemR=new ArrayList<HashMap<String, String>>();
        for(HashMap<String, String> mi :listItem){
            listItemR.add(mi);
        }
        for(HashMap<String, String> m :listItemR){
            if(! m.get("nom").contains(s)){

                listItem.remove(m);
            }
        }
        mListAdapter.notifyDataSetChanged();
    }


    //fonction appelée lorsque on appuie sur le bouton Nouveau Client
    public void addItem(View view){
        //exercice 1
        // Creation de L'intent
        // Intent client  = new Intent(MainActivityListView.this, MainActivityForm.class);
        // L'exécution de L'activity MainActivityForm avec l'intent en paramètre.
        //startActivityForResult(client, IDENT_REQUEST);

        // exercice 2
        Intent SecondB = new Intent(MainActivityListView.this, MainActivityForm.class);
        // démarrer l'activity MainActivityFrom contenant le formulaire
        startActivity(SecondB);

    }


    /*Exercice 2.1 TP4 */
    /*fonction permettant la synchronisation (Server -> Contentprovider)*/
    private void initialiseBDServer(){
        listItem.clear();
        ParseQuery<ParseObject> query = ParseQuery.getQuery("Client");
        query.findInBackground(new FindCallback<ParseObject>() {

            @Override
            public void done(List<ParseObject> postList, com.parse.ParseException e) {

                if (e == null) {
                    // If there are results, update the list of posts
                    // and notify the adapter
                    String columns[] = new String[]{ColumnsTable.Client.Client_NAME, ColumnsTable.Client.Client_PRENOM, ColumnsTable.Client.Client_DATE, ColumnsTable.Client.Client_VILLE};
                    //Uri mContacts = AndroidProvider.CONTENT_URI;
                    // Cursor cur = getContentResolver().query(mContacts, columns, null, null, null);
                    getContentResolver().delete(AndroidProvider.CONTENT_URI, null, null);
                    for (ParseObject post : postList) {

                        /*recupration des donneés à partir de serveur */
                        String nom = post.getString("nom");
                        String prenom = post.getString("prenom");
                        String date = post.getString("date");
                        String ville = post.getString("ville");
                       // getContentResolver().delete(AndroidProvider.CONTENT_URI, null, null);
                        HashMap<String, String> map = new HashMap<String, String>();
                        map.put("nom", nom);
                        Log.d("TAG", post.getString("nom"));
                        //on insère un élément description que l'on récupérera dans le textView description créé dans le fichier affichageitem.xml
                        map.put("prenom", prenom);
                        //on insère un élément titre que l'on récupérera dans le textView titre créé dans le fichier affichageitem.xml
                        map.put("date", date);
                        //on insère un élément titre que l'on récupérera dans le textView titre créé dans le fichier affichageitem.xml
                        map.put("ville", ville);
                        //on insère la référence à l'image (convertit en String car normalement c'est un int) que l'on récupérera dans l'imageView créé dans le fichier affichageitem.xml
                        map.put("img", String.valueOf(R.mipmap.ic_launcher));
                        //enfin on ajoute cette hashMap dans la arrayList
                        listItem.add(map);
                        //Création d'un SimpleAdapter qui se chargera de mettre les items présent dans notre list (listItem) dans la vue affichageitem
                        //if(mListAdapter==null){
                        Log.d("e", "enter adapter");
                        /*insertion des données dans le content provider */
                        ContentValues client = new ContentValues();
                        client.put(ColumnsTable.Client.Client_NAME, nom);
                        client.put(ColumnsTable.Client.Client_PRENOM, prenom);
                        client.put(ColumnsTable.Client.Client_DATE, date);
                        client.put(ColumnsTable.Client.Client_VILLE, ville);
                        getContentResolver().insert(AndroidProvider.CONTENT_URI, client);
                    }
                    mListAdapter.notifyDataSetChanged();
                } else {
                    Log.d(getClass().getSimpleName(), "Error: " + e.getMessage());
                }
            }
        });

    }

    protected class ParseReceiver extends BroadcastReceiver {
        public static final String PROCESS_RESPONSE = "com.example.ader.tp1.intent.action.PROCESS_RESPONSE";
        @Override
        public void onReceive(Context context, Intent intent) {
           initialiseBDServer();
            Log.d("tag","Sync - update - chaque 5s");
        }

    }

}
