package com.example.ader.tp1;

import android.annotation.TargetApi;
import android.content.ContentValues;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.parse.Parse;
import com.parse.ParseObject;

public class MainActivityForm extends AppCompatActivity {

    static Boolean ajouté=false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_zero) {
            EditText nom=(EditText)findViewById(R.id.editText);
            EditText prenom=(EditText)findViewById(R.id.editText2);
            EditText date=(EditText)findViewById(R.id.editText3);
            EditText ville=(EditText)findViewById(R.id.editText4);
            nom.setText(" ");


            return true;
        }
        if(!ajouté) {
            if (id == R.id.action_add) {
                LinearLayout a = (LinearLayout) findViewById(R.id.layout);
                TextView phone = new TextView(this);
                phone.setText("Phone");
                phone.setId(View.generateViewId());
                phone.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
                EditText texte = new EditText(this);
                a.addView(phone, a.getChildCount() - 1);
                a.addView(texte, a.getChildCount() - 1);
                a.invalidate();
                ajouté=true;
            }
        }
        if (id == R.id.action_url) {
            String url = "http://fr.wikipedia.org";
            Intent i = new Intent(Intent.ACTION_VIEW);
            i.setData(Uri.parse(url));
            startActivity(i);
        }
        return super.onOptionsItemSelected(item);
    }
    // fonction appelée lorsque on appuie sur le bouton valider
    public void validate (View view){
        EditText nom=(EditText)findViewById(R.id.editText);
        EditText prenom=(EditText)findViewById(R.id.editText2);
        EditText date=(EditText)findViewById(R.id.editText3);
        EditText ville=(EditText)findViewById(R.id.editText4);
        //exercice 1
        //Intent client = new Intent();
        // l'ajout des informations obtenus par le formulaire
      //  Utilisateur U = new Utilisateur(prenom.getText().toString(),nom.getText().toString(),date.getText().toString(),ville.getText().toString());
       // client.putExtra("Uti",U);
        //setResult(RESULT_OK,client);

        //exercice 2
        ContentValues client = new ContentValues();
        client.put(ColumnsTable.Client.Client_NAME,nom.getText().toString() );
        client.put(ColumnsTable.Client.Client_PRENOM, prenom.getText().toString());
        client.put(ColumnsTable.Client.Client_DATE, date.getText().toString());
        client.put(ColumnsTable.Client.Client_VILLE, ville.getText().toString());

        getContentResolver().insert(AndroidProvider.CONTENT_URI, client);

        // Ajout de Client  sur le Serveur www.parse.com
        final ParseObject post = new ParseObject("Client");
       // post.put(client, client);
        post.put("nom",nom.getText().toString() );
        post.put("prenom",prenom.getText().toString());
        post.put("date",date.getText().toString());
        post.put("ville",ville.getText().toString());
        post.saveInBackground();
        client.clear();
        finish();
    }
}
