package com.example.ader.tp1;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

/**
 * Created by ader on 02/10/15.
 */
public class SecondeActivity extends Activity {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.seconde_activity);

        Intent act = getIntent();
        Bundle truc = act.getExtras();
        Utilisateur p = truc.getParcelable("Uti");

        String prenom = p.getPrenom();
        String nom = p.getNom();
        String date = p.getDate();
        String villee = p.getVille();

        TextView tprenom = (TextView) findViewById(R.id.prenom);
        tprenom.setText("Prenom:"+prenom);
        TextView tnom = (TextView) findViewById(R.id.nom);
        tnom.setText("Nom:" + nom);
        TextView tdate = (TextView) findViewById(R.id.date);
        tdate.setText("Date:"+date);
        TextView tville = (TextView) findViewById(R.id.ville);
        tville.setText("Ville:"+villee);


    }




}
