package com.example.ader.tp1;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by ader on 02/10/15.
 */
public class Utilisateur implements Parcelable {


    private String prenom;
    private String nom;
    private String date;
    private String ville;
    public static final Creator<Utilisateur> CREATOR = new Creator<Utilisateur>() {

        public Utilisateur createFromParcel(Parcel in) {
            return new Utilisateur(in);
        }

        public Utilisateur[] newArray(int size) {
            return new Utilisateur [size];
        }

    };

    public Utilisateur(Parcel in) {
        nom = in.readString();
        prenom=in.readString();
        date = in.readString();
        ville = in.readString();
    }
    public Utilisateur(String prenom ,String nom, String date, String ville) {
        this.nom = nom;
        this.date = date;
        this.ville = ville;
        this.prenom=prenom;
    }


    public void setNom(String nom) {
        this.nom = nom;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }

    public String getNom() {
        return nom;
    }

    public String getDate() {
        return date;
    }

    public String getVille() {
        return ville;
    }
    public String getPrenom() {
        return prenom;
    }



    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(nom);
        dest.writeString(prenom);
        dest.writeString(date);
        dest.writeString(ville);
    }
}
