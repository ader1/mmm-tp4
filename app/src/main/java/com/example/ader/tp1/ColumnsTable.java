package com.example.ader.tp1;

import android.provider.BaseColumns;

/**
 * Created by ader on 16/10/15.
 */
public class ColumnsTable {
    public ColumnsTable(){
    }
    public static final class Client implements BaseColumns {

        private Client() {}

        public static final String Client_ID = "CLIENT";
        public static final String Client_NAME = "CLIENT_NAME";
        public static final String Client_PRENOM = "CLIENT_PRENOM";
        public static final String Client_DATE="CLIENT_DATE";
        public static final String Client_VILLE="CLIENT_VILLE";
    }

}
