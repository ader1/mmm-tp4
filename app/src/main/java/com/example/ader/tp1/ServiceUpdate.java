package com.example.ader.tp1;

import android.app.IntentService;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;

/**
 * Created by ader on 07/12/15.
 */
public class ServiceUpdate extends IntentService {



    public ServiceUpdate(){
        super(ServiceUpdate.class.getName());
    }
    @Override
    protected void onHandleIntent(Intent intent) {
        Intent update = new Intent();
        update.setAction(MainActivityListView.ParseReceiver.PROCESS_RESPONSE);
        update.addCategory(Intent.CATEGORY_DEFAULT);
        while(true){

            try {
                Thread.sleep(5000);
                sendBroadcast(update);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
